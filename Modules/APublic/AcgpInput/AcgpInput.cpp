﻿#include <stdafx.h>
#include "AcgpInput.h"
#include <AcNetwork/AcNetwork.h>
#include <AcuLog/AcuLog.h>

BOOL AcgpInput::OnAddModule()
{
    m_pcsAcgsEventCalendar      = (AcgsEventCalendar*)    getModule("AcgsEventCalendar");
    m_pcsAcgsRegion             = (AcgsRegion*)           getModule("AcgsRegion");

    if (!m_pcsAcgsEventCalendar || !m_pcsAcgsRegion)
        assert(false);

    return TRUE;
}

int AcgpInput::HandleCounterRequest()
{
    if (!m_pcsAcgsEventCalendar->timerIsActive())
        Log("[HandleCounterRequest] Warn: There is no time data set yet. Will be send empty data.", true);

    CustomServerPacketTemplate p = m_pcsAcgsEventCalendar->getCurrentTimerData();

    return Send(&p, sizeof(p)) == 0 ? Log("[HandleCounterRequest]: CustomServerPacketTemplate is sent successfully.") :
        Error("[HandleCounterRequest]: CustomServerPacketTemplate couldn't be sent.");
}

int AcgpInput::HandleRegionRequest(SOCKET server_socket, sockaddr* client_address, int client_address_len, CustomServerPacketTemplate& data)
{
    CustomServerRegionPacket p;
    memset(&p, 0, sizeof(p));
    m_pcsAcgsRegion->getRegionData(p);

    return Send(&p, sizeof(p)) == 0 ? Log("[HandleRegionRequest]: CustomServerRegionPacket is sent successfully."): 
        Error("[HandleRegionRequest]: CustomServerRegionPacket couldn't be sent.");
}

int AcgpInput::ReceiveTimerData(int len, CustomServerPacketTemplate& p)
{
    if (len != sizeof(p))
        return Error("[ReceiveTimerData]: len != sizeof(data).");

    /* Convert timestamp to host byte order. */
    p.timestamp = ntohl(static_cast<uint32_t>(p.timestamp));

    char buff[128];
    _snprintf_s(buff, sizeof(buff), "Event Counter settings is received. Text: %s", p.text);
    Log(buff);

    m_pcsAcgsEventCalendar->setTimerData(p);

    return p.timestamp > 0 || strncmp(p.text, "#Command_Stop_Packet", 16) == 0 ? Log("[ReceiveTimerData]: The data is set to send successfully.") :
        Error("[ReceiveTimerData]: The data couldn't be set.");
}

int AcgpInput::ReceiveRegionData(int len, CustomServerPacketTemplate& p)
{
    if (len != sizeof(p))
        return Error("[ReceiveRegionData]: len != sizeof(data).");
   
    return m_pcsAcgsRegion->addRegionData(static_cast<INT16>(p.timestamp), p.text)? Log("[ReceiveRegionData]: The region data is processed successfully.") :
        Error("[ReceiveRegionData]: The region data could not be processed.");
}

int AcgpInput::HandleTimeRequest(SOCKET server_socket, sockaddr* client_address, int client_address_len)
{
    time_t timestamp = time(0);
    
    return Send(&timestamp, sizeof(timestamp)) == 0 ? Log("[HandleTimeRequest]: The timestamp is sent successfully.") :
        Error("[HandleTimeRequest]: The timestamp couldn't be sent.");
}

int AcgpInput::Analyze(SOCKET& server_socket, sockaddr_in& client_address, int& client_address_len, CustomServerPacketTemplate& data, int& len)
{
    switch (data.header)
    {
        case HEADER_REQUEST_COUNTER_DATA:
            return HandleCounterRequest();
        case HEADER_SET_COUNTER_DATA:
            return ReceiveTimerData(len, data);
        case HEADER_REQUEST_TIME_DATA:
            return HandleTimeRequest(server_socket, (sockaddr*)&client_address, client_address_len);
        case HEADER_SET_REGION:
            return ReceiveRegionData(len, data);
        case HEADER_REQUEST_REGION_DATA:
            return HandleRegionRequest(server_socket, (sockaddr*)&client_address, client_address_len, data);
        default:
            Error("[Analyze]: Unknown packet header.");
    }

    return -1;
}
