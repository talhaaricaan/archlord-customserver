#pragma once
#include <AcNetwork/AcNetwork.h>
#include <AcgsEventCalendar/AcgsEventCalendar.h>
#include <AcgsRegion/AcgsRegion.h>

class AcgpInput : public singleton<AcgpInput>
{
public:
	AcgsEventCalendar* m_pcsAcgsEventCalendar;
	AcgsRegion* m_pcsAcgsRegion;

public:
	AcgpInput() {};
	virtual ~AcgpInput() {};
	
	BOOL	OnAddModule();

public:
	int Analyze(SOCKET& server_socket, sockaddr_in& client_address, int& client_address_len, CustomServerPacketTemplate& data, int& len);

private:
	int HandleCounterRequest();
	int HandleRegionRequest(SOCKET server_socket, sockaddr* client_address, int client_address_len, CustomServerPacketTemplate& data);
	int ReceiveTimerData(int len, CustomServerPacketTemplate& data);
	int ReceiveRegionData(int len, CustomServerPacketTemplate& data);
	int HandleTimeRequest(SOCKET server_socket, sockaddr* client_address, int client_address_len);
};
