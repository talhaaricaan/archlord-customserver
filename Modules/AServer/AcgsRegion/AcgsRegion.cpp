#include "stdafx.h"
#include "AcgsRegion.h"

static std::fstream regionDataFile;

#define AGCMREGION_COMMAND_DISABLE_NAME		"disablename"

static EnumAgcmRegionFlags StringKeyToEnum(const CHAR* szText)
{
	if (strncmp(szText, AGCMREGION_COMMAND_DISABLE_NAME, 5) == 0)
		return E_AGCM_REGION_FLAG_DISABLE_NAME;

	return E_AGCM_REGION_FLAG_NONE;
}

AcgsRegion::AcgsRegion()
{
	ReadRegionDataFile();
}

AcgsRegion::~AcgsRegion()
{
	CloseFile(regionDataFile);
}

BOOL AcgsRegion::OnAddModule()
{
    return TRUE;
}

BOOL AcgsRegion::FlagRegion(INT16 nRegionIndex, EnumAgcmRegionFlags eRegionFlag, BOOL bAddFlag)
{
	/* If the array contains the region. */
	for (auto it = regionArray.begin(); it != regionArray.end(); ++it) {

		INT16& itRegionIndex = it->first;
		UINT16& itRegionFlag = it->second;

		if (itRegionIndex == nRegionIndex) {
			if (bAddFlag)
				itRegionFlag |= eRegionFlag;
			else if (!bAddFlag)
			{
				itRegionFlag &= ~eRegionFlag;
				if (itRegionFlag == 0) regionArray.erase(it);
			}
			return TRUE;
		}
	}
	/* If the array does not contain the region.*/
	regionArray.push_back(std::make_pair(nRegionIndex, eRegionFlag));
	return TRUE;
}

BOOL AcgsRegion::HasFlagRegion(INT16 nRegionIndex, EnumAgcmRegionFlags eRegionFlag)
{
	for (std::size_t i = 0; i < regionArray.size(); ++i) {

		INT16 itRegionIndex = regionArray[i].first;
		UINT16 itRegionFlag = regionArray[i].second;

		if (itRegionIndex == nRegionIndex)
			return (itRegionFlag & eRegionFlag);
	}
	return FALSE;
}

BOOL AcgsRegion::getRegionData(CustomServerRegionPacket& p)
{
	p.regionCount = static_cast<INT16>(regionArray.size());
	
	for (WORD i = 0; i < p.regionCount; i++) {

		p.regionIndexes[i] = regionArray[i].first;
		p.flagContainers[i] = regionArray[i].second;
	
	}
	return TRUE;
}

BOOL AcgsRegion::addRegionData(INT16 nRegionIndex, const CHAR* szFlag)
{
	BOOL bAddFlag = szFlag ? szFlag[0] != '!' : FALSE;

	FlagRegion(nRegionIndex, StringKeyToEnum(bAddFlag ? szFlag : szFlag + 1), bAddFlag);
	return SaveRegionDataFile();
}

BOOL AcgsRegion::ReadRegionDataFile()
{
	if (!OpenFile(regionDataFile, REGION_DAT_FILE_NAME, std::ios::in | std::ios::out | std::ios::app))
	{
		Error("[ReadRegionDataFile]: The file couldn't be opened");
		return FALSE;
	}

	INT16 first;
	UINT16 second;

	while (regionDataFile >> first >> second)
	{
		regionArray.push_back(std::make_pair(first, (UINT16) (second)));
	}

	LogRegionReadFile(regionArray.size());
	return TRUE;
}

BOOL AcgsRegion::SaveRegionDataFile()
{
	ClearFile(regionDataFile, REGION_DAT_FILE_NAME);

	for (const auto& pair : regionArray)
	{
		std::string strBuff = std::to_string(pair.first) + "	" + std::to_string(pair.second);
		WriteFile(regionDataFile, strBuff);
	}

	LogRegionUpdateFile(regionArray.size());
	return TRUE;
}