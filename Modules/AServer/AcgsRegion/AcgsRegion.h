#pragma once
#include <AcuFile/AcuFile.h>
#include <AcuLog/AcuLog.h>

enum EnumAgcmRegionFlags {
	E_AGCM_REGION_FLAG_NONE = 0,
	E_AGCM_REGION_FLAG_DISABLE_NAME = 1 << 0,
};

class AcgsRegion : public singleton<AcgsRegion>
{
public:
	AcgsRegion();
	virtual ~AcgsRegion();

	BOOL OnAddModule();
public:
	BOOL ReadRegionDataFile();
	BOOL SaveRegionDataFile();
public:
	/* to Client */
	BOOL getRegionData(CustomServerRegionPacket& p);
	BOOL addRegionData(INT16 nRegionIndex, const CHAR* szFlag);

	BOOL FlagRegion(INT16 nRegionIndex, EnumAgcmRegionFlags eRegionFlag, BOOL bAddFlag);
	BOOL HasFlagRegion(INT16 nRegionIndex, EnumAgcmRegionFlags eRegionFlag);
private:
	std::vector<std::pair<INT16, UINT16>> regionArray;
};