#include "stdafx.h"
#include "AcgsEventCalendar.h"

AcgsEventCalendar::AcgsEventCalendar()
{
	memset(&m_cspTimerData, 0, sizeof(m_cspTimerData));
}

AcgsEventCalendar::~AcgsEventCalendar()
{
}

BOOL AcgsEventCalendar::OnAddModule()
{
	return TRUE;
}

BOOL AcgsEventCalendar::timerIsActive()
{
	return m_cspTimerData.timestamp != 0;
}

CustomServerPacketTemplate& AcgsEventCalendar::getCurrentTimerData()
{
	return m_cspTimerData;
}

BOOL AcgsEventCalendar::setTimerData(CustomServerPacketTemplate p)
{
	m_cspTimerData = p;
	return TRUE;
}

