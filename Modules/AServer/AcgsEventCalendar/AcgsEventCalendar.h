#pragma once

class AcgsEventCalendar : public singleton<AcgsEventCalendar>
{
public:
	AcgsEventCalendar();
	virtual ~AcgsEventCalendar();

	BOOL	OnAddModule();

public:
	CustomServerPacketTemplate& getCurrentTimerData();
	BOOL	setTimerData(CustomServerPacketTemplate p);

	BOOL	timerIsActive();
private:
	CustomServerPacketTemplate m_cspTimerData;
};