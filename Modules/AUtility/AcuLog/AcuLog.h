#pragma once

#include <AcuFile/AcuFile.h>

BOOL	InitializeLogFile(const char* szFileName);
BOOL	CloseLogFile();

int		Error(const char* szText);
int		Log(const char* szText, BOOL bPrint = FALSE);

/* Methods which are reserved for modules. */
int LogRegionReadFile(size_t data_size);
int LogRegionUpdateFile(size_t data_size);
