#include "stdafx.h"
#include "AcuLog.h"

static std::ofstream packetLogFile;

/* Global console methods. */
static void SetConsoleColour(WORD* Attributes, DWORD Colour)
{
	CONSOLE_SCREEN_BUFFER_INFO Info;
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hStdout, &Info);
	*Attributes = Info.wAttributes;
	SetConsoleTextAttribute(hStdout, Colour);
}

static void ResetConsoleColour(WORD Attributes)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Attributes);
}

//////////////////////////////////////////////////////////////////////

BOOL InitializeLogFile(const char* szFileName)
{
	OpenFile(packetLogFile, szFileName, std::ios_base::app);
	assert(packetLogFile.is_open());

	return TRUE;
}

BOOL CloseLogFile()
{
	return CloseFile(packetLogFile);
}

int Error(const char* szText)
{
	time_t current_time = time(0);
	struct tm time_info;
	localtime_s(&time_info, &current_time);
	char time_str[20];
	strftime(time_str, sizeof(time_str), "%a %b %d %H:%M:%S", &time_info);

	char buff[256];
	snprintf(buff, sizeof(buff), "%s :%s", time_str, szText);

	/* Change console color background. */
	WORD Attributes = 0;
	SetConsoleColour(&Attributes, FOREGROUND_RED | FOREGROUND_INTENSITY);
	std::cout << buff << std::endl;
	ResetConsoleColour(Attributes);
	/* Change console color background end... */

	WriteFile(packetLogFile, buff);

	return -1;
}

int Log(const char* szText, BOOL bPrint)
{
	time_t current_time = time(0);
	struct tm time_info;
	localtime_s(&time_info, &current_time);
	char time_str[20];
	strftime(time_str, sizeof(time_str), "%a %b %d %H:%M:%S", &time_info);

	char buff[256];
	snprintf(buff, sizeof(buff), "%s :%s", time_str, szText);

	if (PRINT_LOG || bPrint) std::cout << buff << std::endl;
	
	return WriteFile(packetLogFile, buff);
}

//////////////////////////////////////////////////////////////////////////////////////

/* Methods which are reserved for modules. */

int LogRegionReadFile(size_t data_size)
{
	
	char buff[128];
	if (data_size != 0)
		_snprintf_s(buff, sizeof(buff), "[ReadRegionDataFile]: RegionDataFile(%s) has been read. RegionIndexes = %d", REGION_DAT_FILE_NAME, static_cast<int>(data_size));
	else
		_snprintf_s(buff, sizeof(buff), "[ReadRegionDataFile]: There is no data in RegionDataFile(%s)", REGION_DAT_FILE_NAME);
	return Log(buff);
}

int LogRegionUpdateFile(size_t data_size)
{
	char buff[128];
	_snprintf_s(buff, sizeof(buff), "[SaveRegionDataFile]: RegionDataFile(%s) is updated. RegionIndexes = %d", REGION_DAT_FILE_NAME, static_cast<int>(data_size));
	return Log(buff, TRUE);
}