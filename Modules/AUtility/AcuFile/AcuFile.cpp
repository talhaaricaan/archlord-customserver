#include "stdafx.h"
#include "AcuFile.h"

/* Open file methods. */
BOOL OpenFile(std::ofstream& file, const char* szFileName, std::ios_base::openmode mode)
{
	file.open(szFileName, mode);
	if (!file.is_open()) return FALSE;
	return TRUE;
}

BOOL OpenFile(std::ifstream& file, const char* szFileName, std::ios_base::openmode mode)
{
	file.open(szFileName, mode);
	if (!file.is_open()) return FALSE;
	return TRUE;
}

BOOL OpenFile(std::fstream& file, const char* szFileName, std::ios_base::openmode mode)
{
	file.open(szFileName, mode);
	if (!file.is_open()) return FALSE;
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////

/* Close file methods. */
BOOL CloseFile(std::ofstream& file)
{
	//file.flush();
	file.close();
	return TRUE;
}

BOOL CloseFile(std::ifstream& file)
{
	file.close();
	return TRUE;
}

BOOL CloseFile(std::fstream& file)
{
	//file.flush();
	file.close();
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////

/* Clear file methods. */
BOOL ClearFile(std::ofstream& file, const char* szFileName)
{
	CloseFile(file);
	/* After an ClearFile method. The file is no longer readable. */
	return OpenFile(file, szFileName, std::ios::out | std::ios::trunc);
}

BOOL ClearFile(std::fstream& file, const char* szFileName)
{
	CloseFile(file);
	/* After an ClearFile method. The file is no longer readable. */
	return OpenFile(file, szFileName, std::ios::out | std::ios::trunc);
}


////////////////////////////////////////////////////////////////////////////////////////

/* Write file methods. */
BOOL WriteFile(std::ofstream& file, char* szBuff)
{
	assert(file.is_open());

	file << szBuff;
	file << "\n";

	file.flush();

	return TRUE;
}

BOOL WriteFile(std::fstream& file, char* szBuff)
{
	assert(file.is_open());

	file << szBuff;
	file << "\n";

	file.flush();

	return TRUE;
}

BOOL WriteFile(std::ofstream& file, std::string str)
{
	assert(file.is_open());

	file << str;
	file << "\n";

	file.flush();

	return TRUE;
}

BOOL WriteFile(std::fstream& file, std::string str)
{
	assert(file.is_open());

	file << str;
	file << "\n";

	file.flush();

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////

/* Read from file methods. */
BOOL ReadFile(std::ifstream& file)
{
	return TRUE;
}

BOOL ReadFile(std::fstream& file)
{
	return TRUE;
}