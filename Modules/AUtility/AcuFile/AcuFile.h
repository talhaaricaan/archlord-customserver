#pragma once

/* Open file methods. */
BOOL OpenFile(std::ofstream		& file, const char* szFileName, std::ios_base::openmode mode);
BOOL OpenFile(std::ifstream		& file, const char* szFileName, std::ios_base::openmode mode);
BOOL OpenFile(std::fstream		& file, const char* szFileName, std::ios_base::openmode mode);

/* Close file methods. */
BOOL CloseFile(std::ofstream	& file);
BOOL CloseFile(std::ifstream	& file);
BOOL CloseFile(std::fstream		& file);

/* Clear file methods. */
BOOL ClearFile(std::ofstream	& file, const char* szFileName);
BOOL ClearFile(std::fstream		& file, const char* szFileName);

/* Write file methods. */
BOOL WriteFile(std::ofstream	& file, char* szBuff);
BOOL WriteFile(std::fstream		& file, char* szBuff); 
BOOL WriteFile(std::ofstream	& file, std::string str);
BOOL WriteFile(std::fstream		& file, std::string str);

/* Read from file methods. */
BOOL ReadFile(std::ifstream		& file);
BOOL ReadFile(std::fstream		& file);