#ifndef __SINGLETON_H__
#define __SINGLETON_H__

template<typename T>
class singleton {
public:
	static T* instance_ptr() {
		static T instance;
		return &instance;
	}

	singleton(singleton const&) = delete;
	void operator=(singleton const&) = delete;

protected:
	singleton() {}
};



#endif
