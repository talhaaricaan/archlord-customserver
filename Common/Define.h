#pragma once

#define PRINT_LOG 1

#define SYS_LOG_FILE_NAME	    "AlcSys.log"
#define REGION_DAT_FILE_NAME    "AlcRegion.dat"

struct CustomServerPacketTemplate {
    BYTE header;
    char text[50];
    time_t timestamp;
    bool flag1;
    bool flag2;
};

struct CustomServerRegionPacket {
    BYTE header;
    INT16 regionCount;
    INT16 regionIndexes[50];
    UINT16 flagContainers[50];
};

enum ETransferData {
    HEADER_REQUEST_COUNTER_DATA,
    HEADER_SET_COUNTER_DATA,
    HEADER_REQUEST_TIME_DATA,
    HEADER_SET_REGION,
    HEADER_REQUEST_REGION_DATA,
};

extern SOCKET* g_ServerSocket;