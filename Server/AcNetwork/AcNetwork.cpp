#include "stdafx.h"
#include "AcNetwork.h"

#define DEFAULT_PORT 12345

static SOCKET*          pServer_Socket;
static sockaddr_in*     pLastConnClientAddress;
static int              pLastConnClientAddressLen;
extern AcgpInput*       g_pcsAcgpInput;

int start_network()
{
    // initialize Winsock
    WSADATA wsa_data;
    int wsa_result = WSAStartup(MAKEWORD(2, 2), &wsa_data);
    if (wsa_result != 0) {
        perror("WSAStartup");
        return -1;
    }

    // create a socket object
    SOCKET server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (server_socket == INVALID_SOCKET) {
        perror("socket");
        WSACleanup();
        return -1;
    }

    // bind the socket to a specific address and port
    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(DEFAULT_PORT);

    int bind_result = bind(server_socket, (sockaddr*)&address, sizeof(address));
    if (bind_result == SOCKET_ERROR) {
        perror("bind");
        closesocket(server_socket);
        WSACleanup();
        return -1;
    }

    pServer_Socket = &server_socket;

    while (true) {
        // receive data from the client
        sockaddr_in client_address;
        int client_address_len = sizeof(client_address);
        CustomServerPacketTemplate data;
        int received = recvfrom(server_socket, (char*)&data, sizeof(data), 0, (sockaddr*)&client_address, &client_address_len);
        if (received < 0) {
            perror("recvfrom");
            continue;
        }

        SaveLastConn(&client_address, client_address_len);
        g_pcsAcgpInput->Analyze(server_socket, client_address, client_address_len, data, received);
    }

    closesocket(server_socket);
    WSACleanup();
    return 0;
}

int Send(const void* packet, int len)
{
    int sent = sendto(*pServer_Socket, (const char*)packet, len, 0, (sockaddr*)pLastConnClientAddress, pLastConnClientAddressLen);
    if (sent < 0) {
        Error("Error: Send to.");
        return -1;
    }
    return 0;
}

VOID SaveLastConn(sockaddr_in* client_address, int client_address_len)
{
    pLastConnClientAddress = client_address;
    pLastConnClientAddressLen = client_address_len;
}