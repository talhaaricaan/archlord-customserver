﻿#include "stdafx.h"
#include <AcNetwork/AcNetwork.h>
#include <AcuLog/AcuLog.h>
#include <AcgpInput/AcgpInput.h>
#include <AcgsEventCalendar/AcgsEventCalendar.h>
#include <AcgsRegion/AcgsRegion.h>

AcgpInput*              g_pcsAcgpInput              = NULL;
AcgsEventCalendar*      g_pcsAcgsEventCalendar      = NULL;
AcgsRegion*             g_pcsAcgsRegion             = NULL;

static int initialize()
{
    InitializeLogFile(SYS_LOG_FILE_NAME);

    /* Initialize Modules. */
    g_pcsAcgpInput              = AcgpInput::instance_ptr();
    g_pcsAcgsEventCalendar      = AcgsEventCalendar::instance_ptr();
    g_pcsAcgsRegion             = AcgsRegion::instance_ptr();
    /* Initialize Modules. End... */

    /* OnAddModule process. */
    g_pcsAcgpInput              ->OnAddModule();
    g_pcsAcgsEventCalendar      ->OnAddModule();
    g_pcsAcgsRegion             ->OnAddModule();
    /* OnAddModule process. End... */

    return 0;
}

void* getModule(const char* szModuleName)
{
    if (strcmp("AcgpInput", szModuleName) == 0)
        return g_pcsAcgpInput;
    else if (strcmp("AcgsEventCalendar", szModuleName) == 0)
        return g_pcsAcgsEventCalendar;
    else if (strcmp("AcgsRegion", szModuleName) == 0)
        return g_pcsAcgsRegion;
    else
        return nullptr;
}

int main() {
    initialize();

    Log("Server is started.", TRUE);
    Log("Listening for the network requests...", TRUE);

    start_network();
    /* Shutdown... */
    CloseLogFile();
    return 0;
}
